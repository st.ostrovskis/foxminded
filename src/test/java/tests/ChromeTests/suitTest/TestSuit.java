package tests.ChromeTests.suitTest;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import tests.ChromeTests.FinalTest.CashFlowTest;
import tests.ChromeTests.FinalTest.ConsultanciesTest;
import tests.ChromeTests.FinalTest.CreateClientFieldTest;
import tests.ChromeTests.FinalTest.DashboardTest;


@RunWith(Suite.class)
@Suite.SuiteClasses({CashFlowTest.class, ConsultanciesTest.class, CreateClientFieldTest.class, DashboardTest.class})

public class TestSuit {
}
