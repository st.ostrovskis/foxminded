package tests.ChromeTests.EmployeeTests;

import config.ChromeDriverSetUp;
import org.junit.Test;
import pages.DashboardPage;
import pages.EmployeesPage;
import pages.LoginPage;
import utils.Log;


public class EmployeesTest extends ChromeDriverSetUp {

    @Test
    public void testCreateEmployee(){

        Log.startTestCase("Employee Test");
        LoginPage loginPage = new LoginPage(driver);

        //Login in page
        DashboardPage dashboardPage = loginPage.login("USERNAME","PASSWORD" );

        //Click on employee button
        EmployeesPage employeesPage = dashboardPage.employeesPage();

        //Print records count
        System.out.println(employeesPage.getRecordsCount());

        //Create  new employee
        employeesPage.createEmployees("Luc","Skywalker","10");

        // Print records count
        System.out.println(employeesPage.getRecordsCount());

        //Search for new created employee
        employeesPage.searchEmployee("Luc Skywalker");

        Log.endTestCase();
    }
}
