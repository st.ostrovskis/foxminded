package tests.ChromeTests.DealsTests;

import config.ChromeDriverSetUp;
import org.junit.Test;
import pages.DashboardPage;
import pages.DealsPage;
import pages.LoginPage;

import static config.DataBase.clientListDB;
import static config.DataBase.getSelectList;
import static pages.DealsPage.dealsClientDBList;
import static testdata.ConfigSQL.clientForDeals;
import static utils.GetAssert.getAssert;

public class DealsTest extends ChromeDriverSetUp {

    @Test
    public void searchDealsClient(){

        LoginPage loginPage = new LoginPage(driver);

        //Login on site
        DashboardPage dashboardPage = loginPage.login("USERNAME", "PASSWORD");

        //Click Deals button
        DealsPage dealsPage = dashboardPage.dealsPage();

        //Get client info from DB(deals)
        getSelectList(clientForDeals, 7);

        //search client by DB info on site
        dealsPage.searchClient();

        getAssert(clientListDB, dealsClientDBList);
    }
}
