package tests.ChromeTests.ConsultanciesTests;

import config.ChromeDriverSetUp;
import org.junit.Test;
import pages.ConsultanciesPage;
import pages.DashboardPage;
import pages.LoginPage;
import utils.Log;

import static utils.GetRandom.getRandomInt;
import static utils.GetRandom.getRandomString;
import static utils.ScreenShootCapture.captureScreenShot;

public class NegativeNewConsultancyTest extends ChromeDriverSetUp {

    public static String description;

    @Test

    public void negativeConsultancyTest(){
        Log.startTestCase("Negative new consultancy Test");

        //generate test data
        String title = getRandomString(51);
        String description = getRandomString(0);
        String priceUAH = String.valueOf(getRandomInt(4));
        String priceEUR = String.valueOf(getRandomInt(3));
        String priceUSD = String.valueOf(getRandomInt(3));
        String employeeRate = String.valueOf(getRandomInt(1));


        //open web page
        LoginPage loginPage = new LoginPage(driver);

        //Login on site
        DashboardPage dashboardPage = loginPage.login("USERNAME","PASSWORD");

        //Click on Consultancy button
        ConsultanciesPage consultanciesPage = dashboardPage.consultanciesPage();

        //create new consultancy
        consultanciesPage.createNewConsultancy(title, description, priceUAH, priceEUR, priceUSD, employeeRate);

        captureScreenShot(driver,"Negative New Consultancy Test");

        //check description message
        consultanciesPage.checkDescriptionMessage("It is required field");

        //check title message
        consultanciesPage.checkTitleMessage("size must be between 2 and 50");

        Log.endTestCase();
    }
}
