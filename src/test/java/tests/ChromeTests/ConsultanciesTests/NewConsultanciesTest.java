package tests.ChromeTests.ConsultanciesTests;

import config.ChromeDriverSetUp;
import org.junit.Test;
import pages.ConsultanciesPage;
import pages.DashboardPage;
import pages.LoginPage;
import utils.Log;

import static utils.GetRandom.getRandomInt;
import static utils.GetRandom.getRandomString;
import static utils.ScreenShootCapture.captureScreenShot;


public class NewConsultanciesTest extends ChromeDriverSetUp {

    @Test
    public void testCreateConsultancy() {
        Log.startTestCase("Consultancy Test");

        //generate test data
        String title = getRandomString(10);
        String description = getRandomString(20);
        String priceUAH = String.valueOf(getRandomInt(4));
        String priceEUR = String.valueOf(getRandomInt(3));
        String priceUSD = String.valueOf(getRandomInt(3));
        String employeeRate = String.valueOf(getRandomInt(1));

        LoginPage loginPage = new LoginPage(driver);

        //Login in page
        DashboardPage dashboardPage = loginPage.login("USERNAME", "PASSWORD");

        //Click on consultancy button
        ConsultanciesPage consultanciesPage = dashboardPage.consultanciesPage();

        //Print records count
        System.out.println(consultanciesPage.getRecordsCount());

        //Create new consultancy
        consultanciesPage.createNewConsultancy(title, description, priceUAH, priceEUR, priceUSD, employeeRate);

        //Print records count
        System.out.println(consultanciesPage.getRecordsCount());

        //Search for new created consultancy
        consultanciesPage.searchConsultancy(title);

        captureScreenShot(driver, "New Consultancy Test");


        Log.endTestCase();
    }
}
