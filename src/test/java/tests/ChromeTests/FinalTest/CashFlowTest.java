package tests.ChromeTests.FinalTest;

import config.ChromeDriverSetUp;
import org.junit.Test;
import pages.CashflowPage;
import pages.DashboardPage;
import pages.LoginPage;
import utils.Log;

import static utils.ScreenShootCapture.captureScreenShot;

public class CashFlowTest extends ChromeDriverSetUp {

    @Test
    public void cashFlowReport(){
        Log.startTestCase("Cash Flow Test");
        //open website
        LoginPage loginPage = new LoginPage(driver);

        //login on website
        DashboardPage dashboardPage = loginPage.login("USERNAME","PASSWORD");

        //click on cash flow button
        CashflowPage cashflowPage = dashboardPage.cashflowPage();

        //make report
        cashflowPage.makeReport();

        captureScreenShot(driver,"Make Report Test");

        Log.endTestCase();
    }
}
