package tests.ChromeTests.FinalTest;

import config.ChromeDriverSetUp;
import org.junit.Test;
import pages.DashboardPage;
import pages.LoginPage;
import utils.Log;

import static utils.ScreenShootCapture.captureScreenShot;

public class DashboardTest extends ChromeDriverSetUp {

    @Test

    public void dashBoardTest(){
        Log.startTestCase("Dashboard Test");

        //open website
        LoginPage loginPage =new LoginPage(driver);

        //login on website
        DashboardPage dashboardPage = loginPage.login("USERNAME","PASSWORD");

        //Check that we on Dashboard page
        dashboardPage.checkDashboard();

        captureScreenShot(driver, "DashboardTest");

        Log.endTestCase();

    }
}
