package tests.ChromeTests.FinalTest;

import config.ChromeDriverSetUp;
import org.junit.Test;
import pages.DashboardPage;
import pages.ExtraFields;
import pages.LoginPage;
import utils.Log;

import static utils.GetRandom.getRandomString;
import static utils.ScreenShootCapture.captureScreenShot;

public class CreateClientFieldTest extends ChromeDriverSetUp {


    @Test
    public void createClientFieldTest(){
        Log.startTestCase("Create new client field");

        String newFieldName = getRandomString(4);

        //Open website
        LoginPage loginPage = new LoginPage(driver);

        //Login on website
        DashboardPage dashboardPage = loginPage.login("USERNAME","PASSWORD");

        //Click on Extra Fields button
        ExtraFields extraFields = dashboardPage.extraFields();

        //Create new client field
        extraFields.createNewClientField(newFieldName);

        //Search new created field
        extraFields.searchClientField(newFieldName);

        captureScreenShot(driver, "Create new client field");

        //delete new created client field
        extraFields.deleteClientField(newFieldName);

        Log.endTestCase();
    }
}
