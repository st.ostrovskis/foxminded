package tests.ChromeTests.FinalTest;

import config.ChromeDriverSetUp;
import org.junit.Test;
import pages.ConsultanciesPage;
import pages.DashboardPage;
import pages.LoginPage;
import utils.Log;

import static utils.GetRandom.getRandomInt;
import static utils.GetRandom.getRandomString;
import static utils.ScreenShootCapture.captureScreenShot;

public class ConsultanciesTest extends ChromeDriverSetUp {

    @Test

    public void newConsultanciesTest(){
        Log.startTestCase("New Consultancies Test");

        //generate test data
        String title = getRandomString(10);
        String description = getRandomString(20);
        String priceUAH = String.valueOf(getRandomInt(4));
        String priceEUR = String.valueOf(getRandomInt(3));
        String priceUSD = String.valueOf(getRandomInt(3));
        String employeeRate = String.valueOf(getRandomInt(1));

        //Open website
        LoginPage loginPage = new LoginPage(driver);

        //Login on website
        DashboardPage dashboardPage = loginPage.login("USERNAME","PASSWORD");

        //Click on Consultancies button
        ConsultanciesPage consultanciesPage = dashboardPage.consultanciesPage();

        //Create new Consultancy
        consultanciesPage.createNewConsultancy(title, description, priceUAH, priceEUR, priceUSD, employeeRate);

        //Search for new created consultancy
        consultanciesPage.searchConsultancy(title);

        captureScreenShot(driver, "New Consultancy Test");

        Log.endTestCase();
    }

}
