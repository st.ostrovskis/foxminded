package tests.ChromeTests.InvoicesTests;

import config.ChromeDriverSetUp;
import org.junit.Test;
import pages.DashboardPage;
import pages.InvoicesPage;
import pages.LoginPage;
import utils.Log;
import utils.ScreenShootCapture;

import static config.DataBase.clientListDB;
import static config.DataBase.getSelectList;
import static pages.InvoicesPage.clientInvoiceList;
import static testdata.ConfigSQL.clientForInvoices;
import static utils.GetAssert.getAssert;

public class InvoiceTest extends ChromeDriverSetUp {

    @Test
    public void clientCheckTest(){

        Log.startTestCase("clientCheckTest");

        LoginPage loginPage = new LoginPage(driver);

        DashboardPage dashboardPage = loginPage.login("USERNAME", "PASSWORD");

        InvoicesPage invoicesPage = dashboardPage.invoicesPage();

        getSelectList(clientForInvoices, 7);

        invoicesPage.searchClient();

        ScreenShootCapture.captureScreenShot(driver, "InvoiceTest");

        getAssert(clientListDB,clientInvoiceList);


        Log.endTestCase();

    }
}
