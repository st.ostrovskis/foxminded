package tests.ChromeTests.InvoicesTests;

import config.ChromeDriverSetUp;
import org.junit.Test;
import pages.ClientsPage;
import pages.DashboardPage;
import pages.InvoicesPage;
import pages.LoginPage;
import utils.Log;

import static config.DataBase.getSelectList;
import static testdata.ConfigSQL.getClientInvoiceAddFunds;

public class AddFundsTest extends ChromeDriverSetUp {

    @Test

    public void addFundsTest(){
        Log.startTestCase("AddFundsTest");

        //open website
        LoginPage loginPage = new LoginPage(driver);

        //login on site
        DashboardPage dashboardPage = loginPage.login("USERNAME", "PASSWORD");

        InvoicesPage invoicesPage = dashboardPage.invoicesPage();

        getSelectList(getClientInvoiceAddFunds, 2);

        ClientsPage clientsPage = invoicesPage.clientsPage();

        clientsPage.addFunds();


    }
}
