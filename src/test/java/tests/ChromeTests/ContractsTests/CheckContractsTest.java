package tests.ChromeTests.ContractsTests;


import config.ChromeDriverSetUp;
import org.junit.Test;
import pages.ContractsPage;
import pages.DashboardPage;
import pages.LoginPage;
import utils.Log;

import static config.DataBase.*;
import static pages.ContractsPage.contractClientList;
import static testdata.ConfigSQL.forContaracts;
import static utils.GetAssert.getAssert;

public class CheckContractsTest extends ChromeDriverSetUp {

    @Test
    public void checkContracts(){

        Log.startTestCase("Check Contract Test");

        LoginPage loginPage = new LoginPage(driver);

        //Login in page
        DashboardPage dashboardPage = loginPage.login("USERNAME", "PASSWORD");

        //Click on Contracts button
        ContractsPage contractsPage = dashboardPage.contractsPage();

        //Get random client info from DB contract
        getSelectList(forContaracts,8);

        //Search on site client with same date as in BD
        contractsPage.searchClient();

        // Assert BD client info with site client info
        getAssert(clientListDB, contractClientList);


        Log.endTestCase();

    }

}
