package tests.ChromeTests.ClientsTests;

import config.ChromeDriverSetUp;
import org.junit.Test;
import pages.ClientsPage;
import pages.DashboardPage;
import pages.LoginPage;
import utils.Log;

import static config.DataBase.clientMapDB;
import static config.DataBase.getSelectMap;
import static pages.ClientsPage.actualInfo;
import static pages.ClientsPage.clientNew;
import static testdata.ConfigSQL.clientSearch;
import static utils.GetAssert.getAssert;
import static utils.ScreenShootCapture.captureScreenShot;


public class ClientsTest extends ChromeDriverSetUp {


    @Test
    public void testCreateClient(){
        Log.startTestCase("ClientTest");

        //open website
        LoginPage loginPage = new LoginPage(driver);

        // Login on site
        DashboardPage dashboardPage = loginPage.login("USERNAME","PASSWORD");

        //Click on clients button
        ClientsPage clientsPage = dashboardPage.clientsPage();

        //Print records count
        System.out.println(clientsPage.getRecordsCount());

        //Create new client
        clientsPage.createNewClient();

        //print records count
        System.out.println(clientsPage.getRecordsCount());

        //Search for new created client and create Map with actual info
        clientsPage.getNewClientInfo();

        //Assert actual info with expected
        getAssert(clientNew, actualInfo);

        //get new created client Info from DB
        getSelectMap(clientSearch, 7);

        //Assert DB info about created client and
        getAssert(actualInfo, clientMapDB);

        captureScreenShot(driver, "Client Create Test");

        Log.endTestCase();

    }
}
