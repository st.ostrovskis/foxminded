package tests.ChromeTests.ClientsTests;

import config.ChromeDriverSetUp;
import org.junit.Test;
import pages.ClientsPage;
import pages.DashboardPage;
import pages.LoginPage;
import utils.Log;

import static config.DataBase.clientMapDB;
import static config.DataBase.getSelectMap;
import static pages.ClientsPage.actualInfo;
import static pages.ClientsPage.clientNew;
import static testdata.ConfigSQL.clientSearch;
import static utils.GetAssert.getAssert;

public class UpdateClientTest extends ChromeDriverSetUp {

    @Test
    public void checkClient(){
        Log.startTestCase("Client Update Test");

        //open website
        LoginPage loginPage = new LoginPage(driver);

        //Login on website
        DashboardPage dashboardPage = loginPage.login("USERNAME","PASSWORD");

        // Click on client button
        ClientsPage clientsPage = dashboardPage.clientsPage();

        // Create new client
        clientsPage.createNewClient();

        //Search new created client
        clientsPage.getNewClientInfo();


        //Change client Info
        clientsPage.changeClientInfo();

        // Get client info from DB
        getSelectMap(clientSearch,7);

        //again search new created client
        clientsPage.getNewClientInfo();

        //Assert changed client info with web client info
        getAssert(clientNew, actualInfo);


        //Assert changed client info adn DB info
        getAssert(actualInfo, clientMapDB);

        Log.endTestCase();
    }

}
