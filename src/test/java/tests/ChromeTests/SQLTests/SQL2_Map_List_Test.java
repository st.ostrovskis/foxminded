package tests.ChromeTests.SQLTests;

import config.ChromeDriverSetUp;
import org.junit.Test;
import utils.Log;

import static config.DataBase.getSelectList;
import static config.DataBase.getSelectMap;
import static testdata.ConfigSQL.clientGroupQueue;
import static testdata.ConfigSQL.clientGroupQueue1;

public class SQL2_Map_List_Test extends ChromeDriverSetUp {

    @Test
    public void getSQLListTest(){
        Log.startTestCase("SQL List Test");

        System.out.println(getSelectMap(clientGroupQueue,2));

        Log.endTestCase();
    }

    @Test
    public void  getSQLMapTest(){
        Log.startTestCase("SQL Map Test");

        System.out.println(getSelectList(clientGroupQueue, 9));

        Log.endTestCase();

    }

    @Test
    public void getExampleTest(){
        Log.startTestCase("SQL Example Test");

        System.out.println(getSelectMap(clientGroupQueue1, 4));

        Log.endTestCase();

    }
}
