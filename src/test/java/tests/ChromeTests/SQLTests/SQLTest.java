package tests.ChromeTests.SQLTests;

import config.ChromeDriverSetUp;
import org.junit.Test;
import utils.Log;

import static config.DataBase.*;
import static testdata.ConfigSQL.*;

public class SQLTest extends ChromeDriverSetUp {

    @Test
    public void setConnection() {
        Log.startTestCase("SQL Test");

        System.out.println(getSelect(clientRandomMonth, 2));
        System.out.println(getSelect(clientActiveRandom, 2));
        System.out.println(getSelect(clientSearch,6));

        Log.endTestCase();
    }
}
