package tests.ChromeTests.LoginRegistrationTests;

import config.ChromeDriverSetUp;
import org.junit.Test;
import pages.LoginPage;
import utils.Log;

import static utils.ScreenShootCapture.captureScreenShot;

public class RegistrationTest extends ChromeDriverSetUp {

    @Test

    public void checkRegister(){
        Log.startTestCase("RegistrationTest");

        //open website
        LoginPage loginPage = new LoginPage(driver);

        //Try to registered
        loginPage.registration("username", "admin", "admin");

        captureScreenShot(driver, "New Registration Test");

        //check email validation message
        loginPage.checkEmailMessage("Please include an '@...");

        Log.endTestCase();

    }
}
