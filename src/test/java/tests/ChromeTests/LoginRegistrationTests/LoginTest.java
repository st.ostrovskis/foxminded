package tests.ChromeTests.LoginRegistrationTests;

import config.ChromeDriverSetUp;
import org.junit.Test;
import pages.DashboardPage;
import pages.LoginPage;
import utils.Log;

import static utils.ScreenShootCapture.captureScreenShot;


public class LoginTest extends ChromeDriverSetUp {

    @Test
    public void testLogin() {
        Log.startTestCase("LoginTest");

        LoginPage login = new LoginPage(driver);

        //Login in page
        DashboardPage dashboardPage = login.login("USERNAME","PASSWORD");

        captureScreenShot(driver, "Login Test");

        Log.endTestCase();
    }
}
