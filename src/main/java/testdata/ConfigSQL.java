package testdata;

import static config.DataBase.clientListDB;
import static pages.ClientsPage.clientNew;
import static pages.InvoicesPage.clientNameForFund;

public class ConfigSQL {
    public static String clientRandomMonth = "SELECT first_name, last_name  FROM public.client " +
            "INNER JOIN deal ON client.id = deal.client_id " +
            "INNER JOIN contract ON deal.id = contract.deal_id " +
            "WHERE EXTRACT(MONTH FROM contract_date) > 6 " +
            "AND EXTRACT(MONTH FROM contract_date) <11 " +
            "ORDER BY RANDOM() " +
            "LIMIT 1";

    public static String clientActiveRandom = "SELECT first_name, last_name FROM public.client " +
            "INNER JOIN public.deal ON deal.client_id = client.id " +
            "WHERE status = 'ACTIVE' " +
            "ORDER BY RANDOM() " +
            "LIMIT 1";

    public static String employeeMaxRandom = "SELECT first_name, last_name, max_clients FROM public.employee " +
            "WHERE max_clients > 10 " +
            "ORDER BY RANDOM() " +
            "LIMIT 1";

    public static String clientGroupQueue = "SELECT client.id, consultancy.name, client.first_name, client.last_name, status, deal.created_date, queuing_date, close_date, priority " +
            "FROM (((public.deal " +
            "LEFT JOIN client ON client.id = deal.client_id) " +
            "LEFT JOIN consultancy  ON deal.consultancy_id = consultancy.id) " +
            "LEFT JOIN deal_queue ON deal.id = deal_queue.deal_id) " +
            "WHERE consultancy.name = 'Group Development' " +
            "AND EXTRACT(MONTH FROM queuing_date) = 11 ";

    public static String clientGroupQueue1 = "SELECT consultancy.name, client.first_name, client.last_name, status, deal.created_date, queuing_date, close_date, priority " +
            "FROM (((public.deal " +
            "LEFT JOIN client ON client.id = deal.client_id) " +
            "LEFT JOIN consultancy  ON deal.consultancy_id = consultancy.id) " +
            "LEFT JOIN deal_queue ON deal.id = deal_queue.deal_id) ";

    public static String clientSearch = "SELECT first_name, last_name, email, country, city, skype, phone_number " +
            "FROM public.client " +
            "WHERE client.last_name = '" + clientNew.get("last_name") +"'";

    public static String forContaracts = "SELECT client.first_name , client.last_name , consultancy.name \"Consultancy\", " +
            "employee.first_name, employee.last_name , contract_date \"Contract date\", payment_type \"Type\", status \"Status\" "+
            "FROM public.contract " +
            "LEFT JOIN public.deal ON deal.id = contract.deal_id " +
            "LEFT JOIN public.client ON client.id = deal.client_id " +
            "LEFT JOIN public.consultancy ON consultancy.id = deal.consultancy_id " +
            "LEFT JOIN public.employee ON employee.id = contract.employee_id " +
            "ORDER BY RANDOM() LIMIT 1 ";

    public static String clientForInvoices = "SELECT invoice.id, client.first_name, client.last_name, contract.id, " +
            "money.currency, money.amount, date_paid  FROM public.invoice " +
            "LEFT JOIN public.contract ON contract.id = invoice.contract_id " +
            "LEFT JOIN public.deal ON deal.id = contract.deal_id " +
            "LEFT JOIN public.client ON client.id = deal.client_id " +
            "LEFT JOIN public.payment ON invoice.id = payment.invoice_id " +
            "LEFT JOIN public.money ON contract.price_id = money.id " +
            "ORDER BY RANDOM() LIMIT 1 ";

    public static String clientForDeals = "SELECT deal.id, client.first_name, client.last_name, consultancy.name, status, open_date, close_date   FROM public.deal " +
            "LEFT JOIN public.client ON deal.client_id = client.id " +
            "LEFT JOIN public.consultancy ON consultancy.id = deal.consultancy_id " +
            "ORDER BY RANDOM() LIMIT 1";

    public static String getClientInvoiceAddFunds = "SELECT client.first_name, client.last_name FROM public.invoice " +
            "LEFT JOIN public.contract ON contract.id = invoice.contract_id " +
            "LEFT JOIN public.deal ON deal.id = contract.deal_id " +
            "LEFT JOIN public.client ON client.id = deal.client_id " +
            "LEFT JOIN public.payment ON invoice.id = payment.invoice_id " +
            "LEFT JOIN public.money ON contract.price_id = money.id " +
            "ORDER BY RANDOM() LIMIT 1";

    public static String getAmountForFundsTest = "SELECT client.first_name, client.last_name, money.amount, " +
            " money.currency FROM public.personal_account_money_transfer " +
            "LEFT JOIN public.personal_account ON personal_account.id = personal_account_money_transfer.personal_account_id " +
            "LEFT JOIN public.client ON client.id = personal_account.id " +
            "LEFT JOIN public.money ON personal_account_money_transfer.money_id = money.id " +
            "ORDER BY money.id DESC LIMIT 1" ;

}

