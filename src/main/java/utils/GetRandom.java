package utils;

import java.util.Random;

public class GetRandom {

    //Create random Integer from minNumber to maxNumber
    public static int getRandomInt(int length){
       Random random = new Random();
        int min = (int) Math.pow(10, length - 1);
        int max = (int) Math.pow(10, length);

        return random.nextInt(max - min) + min;
    }


    //Create random boolean

    public static boolean getRandomBoolean(){
        Random random = new Random();
        return random.nextBoolean();
    }

    //Create String(contain 10 random characters)
    public static String getRandomString(int stringLength){
        int leftLimit = 97;
        int rightLimit = 122;
        Random random = new Random();
        StringBuilder buffer = new StringBuilder(stringLength);
        for (int i = 0; i < stringLength; i++){
            int randomLimitedInt = leftLimit + (int)(random.nextFloat() * (rightLimit - leftLimit + 1 ));
            buffer.append((char)(randomLimitedInt));
        }
        return buffer.toString();

    }
}
