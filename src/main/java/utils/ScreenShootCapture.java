package utils;


import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class ScreenShootCapture {
    WebDriver driver;


    public static void captureScreenShot(WebDriver driver) {
        File src =((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd/HH:mm:ss");
        LocalDateTime now = LocalDateTime.now();
        try {
            FileUtils.copyFile(src, new  File("ScreenShots/" + dtf.format(now) + ".png"));
        }catch (IOException e){
            System.out.println(e.getMessage());
        }
    }

    public static void captureScreenShot(WebDriver driver,String screenShotName) {
        File src =((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd/HH-mm-ss ");
        LocalDateTime now = LocalDateTime.now();
        try {
            FileUtils.copyFile(src, new  File("ScreenShots/" + dtf.format(now) + screenShotName + ".png"));
        }catch (IOException e){
            System.out.println(e.getMessage());
        }
    }
}
