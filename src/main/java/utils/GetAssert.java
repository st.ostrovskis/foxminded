package utils;

import org.junit.Assert;

public class GetAssert {

    //Check client info expected and actual
    public static void getAssert(Object expected, Object actual) {


        if (expected.equals(actual)){
            Log.info("TEST SUCCESSFULLY COMPLETED");
        }else {
            Log.error("TEST FAILED");
            Log.endTestCase();
            Assert.assertEquals("TEST FAILED",expected,actual);
            Assert.fail();

        }
    }
}


