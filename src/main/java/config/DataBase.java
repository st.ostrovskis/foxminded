package config;

import utils.Log;

import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static config.ConfigData.*;
import static config.ReadPropertiesFile.configFileReader;
import static config.ReadPropertiesFile.properties;

public class DataBase {
    private static Connection connection;

    public static  List<String> clientListDB = new ArrayList<>();
    public static  Map<String,String> clientMapDB = new HashMap<>();




    /**
     * Get data base credentials
     * set connection
     */
    public static void setDBConnection() {

        getDBUrl();
        getDBUser();
        getDbPassword();

        try {
            Class.forName("org.postgresql.Driver");
        } catch (ClassNotFoundException e) {
            Log.error("PostgreSQL JDBC Driver is not found. Include it in your library path ");
            e.printStackTrace();
            return;
        }
        Log.info("Connection to PostgreSQL JDBC");

        try {
            connection = DriverManager.getConnection(DB_URL, DB_USER, DB_PASSWORD);
        } catch (SQLException e) {
            Log.error("Connection failed");
            e.printStackTrace();
            return;
        }

        if (connection != null) {
            Log.info("You successfully connected to database now");
        } else {
            Log.error("Failed to make connection to database");

        }
    }

    /**
     * Make request to DB and return String answer
     *
     * @param sqlRequest
     * @param columnCount
     * @return String answer
     */
    public static String getSelect(String sqlRequest, int columnCount) {
        String answer = "";
        setDBConnection();
        try {
            Statement stmt = connection.createStatement();
            ResultSet rs = stmt.executeQuery(sqlRequest);
            while (rs.next()) {
                for (int i = 1; i <= columnCount; i++) {
                    answer += rs.getString(i) + " ";
                }
            }
            rs.close();
            stmt.close();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }

        }
        return answer;
    }

    /**
     * Make request to DB and return List of result
     * @param sqlRequest
     * @param columnCount
     * @return
     */
    public static List<String> getSelectList(String sqlRequest, int columnCount) {
        setDBConnection();
        try {
            Statement stmt = connection.createStatement();
            ResultSet rs = stmt.executeQuery(sqlRequest);
            while (rs.next()) {
                String temp = "";
                for (int i = 1; i <= columnCount; i++) {
                    temp = rs.getString(i) + " ";
                    clientListDB.add(temp);
                }

            }

            rs.close();
            stmt.close();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        Log.info("Get dates from DB");
        return clientListDB;
    }

    public static Map<String, String> getSelectMap(String sqlRequest, int columnCount){
        setDBConnection();
        try {
            Statement stmt = connection.createStatement();
            ResultSet rs = stmt.executeQuery(sqlRequest);
            while (rs.next()) {
                String value = "";
                String key = "";
                for (int i = 1; i <= columnCount; i++) {
                    key = rs.getMetaData().getColumnName(i);
                    value = rs.getString(i);
                    clientMapDB.put(key, value);
                }
            }
            rs.close();
            stmt.close();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return clientMapDB;
    }




}

