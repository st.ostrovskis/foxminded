package config;

import org.junit.After;
import org.junit.Before;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import utils.ScreenShootCapture;

import java.util.concurrent.TimeUnit;

import static config.ConfigData.*;


public class ChromeDriverSetUp {
    protected static  WebDriver driver;

    @Before
    public void setChromeDriverUp() {
        getURL();
        getChromePath();
        System.setProperty("webdriver.chrome.driver", CHROME_PATH);
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
        driver.get(URL);
    }

    @After
    public void cleanUp(){
        driver.manage().deleteAllCookies();
        driver.quit();
    }


}
