package config;

import static config.ReadPropertiesFile.*;

public class ConfigData {
    public static String URL;
    public static String CHROME_PATH;
    public static String FIREFOX_PATH;
    protected static String DB_URL;
    protected static String DB_USER;
    protected static String DB_PASSWORD;



    public static String getURL(){
        configFileReader();
        URL = properties.getProperty("URL");
        return URL;

    }

    public static String getChromePath(){
        configFileReader();
        CHROME_PATH = properties.getProperty("CHROME_PATH");
        return CHROME_PATH;
    }

    public static String getFirefoxPath(){
        configFileReader();
        FIREFOX_PATH = properties.getProperty("FIREFOX_PATH");
        return FIREFOX_PATH;
    }

    protected static String getDBUrl() {
        configFileReader();
        return DB_URL = properties.getProperty("DB_URL");
    }

    protected static String getDBUser() {
        configFileReader();
        return DB_USER = properties.getProperty("DB_USER");
    }

    protected static String getDbPassword() {
        configFileReader();
        return DB_PASSWORD = properties.getProperty("DB_PASSWORD");
    }






}
