package pages;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import utils.Log;

import java.util.List;

/**
 * Page Object encapsulates the Cashflow page.
 */
public class CashflowPage {
    protected WebDriver driver;

    private By makeReportButtonBy = By.xpath("//button[@type='submit']");

    private By consultancyFieldBy = By.id("selectedConsultancyField");

    private By beginDateFieldBy = By.id("beginDateField");

    private By endDateFieldBy = By.id("endDateField");

    private By reportDates = By.xpath("//div[@class='col-md-6']");

    public CashflowPage(WebDriver driver){
        this.driver = driver;
    }

    //make report
    public void makeReport(){
        driver.findElement(makeReportButtonBy).click();
       List<WebElement> report = new WebDriverWait(driver,5).until(ExpectedConditions.presenceOfAllElementsLocatedBy(reportDates));
       if(report != null){
           Log.info("Make report");
       }else {
           Log.error("Cannot make report");
       }
    }




}
