package pages;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import utils.Log;
import utils.ScreenShootCapture;

import java.sql.Driver;
import java.util.*;;

import static config.DataBase.clientListDB;
import static config.DataBase.getSelectList;
import static testdata.ConfigSQL.getAmountForFundsTest;
import static utils.GetAssert.getAssert;
import static utils.GetRandom.getRandomInt;
import static utils.GetRandom.getRandomString;


/**
 * Page Object encapsulates the Clients page.
 */

public class ClientsPage {
    protected WebDriver driver;
    public static Map<String,String> clientNew = new HashMap<>();
    public  static Map<String,String> actualInfo = new HashMap<>();

    private By createClientButtonBy = By.xpath("//div[@class='fixed-table-toolbar']//button");

    private By firstNameBy = By.id("firstName");

    private By lastNameBy = By.id("lastName");

    private By saveButtonBy = By.xpath("//div[@class='modal-body']//button");

    private By emailField = By.id("email");

    private By countryField = By.id("country");

    private By cityField = By.id("city");

    private By phoneNumberField = By.id("phoneNumber");

    private By skypeField = By.id("skype");

    private By searchFieldBy = By.xpath("//div[@class='pull-right search']/input");

    private By refreshButtonBy = By.name("refresh");

    private By clientsNameBy = By.xpath("//table[@class='table table-hover']//td/following-sibling::td");

    private By clientNameToClick =By.xpath("//table[@class='table table-hover']//td/following-sibling::td/a");

    private By clientsRecordCountBy = By.className("pagination-info");

    private By saveCreatedClientButton = By.xpath("//div[@class='btn-group']/button [@type='submit']");

    private By goBackbutton = By.xpath("//button[contains(text(), 'Go back')]");

    private By addFundsButtonBy = By.xpath("//button[@class = 'btn btn-success btn-sm']");

    private By fundsValueBy = By.xpath("//form[@id = 'depositForm']//input");

    private By descriptionBy = By.id("depositDescriptionModal");

    private By saveChangesButtonBy = By.xpath("//div[@id = 'fixed-table-toolbar--buttons-client']//button [@type='button submit']");

    private By amountUAHBy = By.xpath("//td[contains(text(), 'UAH')]/following-sibling::td");

    private By amountEURBy = By.xpath("//td[contains(text(), 'EUR')]/following-sibling::td");

    private By amountUSDBy = By.xpath("//td[contains(text(), 'USD')]/following-sibling::td");

    private By currencyUAH = By.xpath("//table[@data-toggle = 'table']//tbody/tr/td");




    public ClientsPage(WebDriver driver){
        this.driver = driver;
    }

    //Create random Client name
    public  void createClientName(int nameLength) {
        String firstname = getRandomString(nameLength);
        clientNew.put("first_name", firstname);
    }

    //Create random client lastname
    public  void createClientLastname(int lastnameLength){
        String lastname = getRandomString(lastnameLength);
        clientNew.put("last_name",lastname);
    }

    //Create random client email, first part before @, second part after @
    public void createClientEmail(int firstPartLength, int secondPartLength) {
        String email = getRandomString(firstPartLength) + "@" + getRandomString(secondPartLength) + ".com";
        clientNew.put("email", email);
    }

    //Create random client country
    public void createClientCountry(int countryLength) {
        String country = getRandomString(1).toUpperCase() + getRandomString(countryLength - 1);
        clientNew.put("country", country);
    }

    //Create random client city
    public void createClientCity(int cityLength) {
        String city = getRandomString(1).toUpperCase() + getRandomString(cityLength - 1);
        clientNew.put("city", city);
    }

    //Create random client phone number
    public void createClientPhoneNumber(int phoneNumberLength) {
        String phoneNumber = Integer.toString(getRandomInt(phoneNumberLength));
        clientNew.put("phone_number", phoneNumber);
    }

    //Create random client skype
    public void createClientSkype(int stringLength, int numberLength){
        String skype = getRandomString(stringLength) + Integer.toString(getRandomInt(numberLength));
        clientNew.put("skype", skype);
    }

    /**
     * Create new client
     */
    public void createNewClient(){
        createClientName(5);
        createClientLastname(10);
        createClientEmail(5,8);
        createClientCountry(8);
        createClientCity(6);
        createClientPhoneNumber(10);
        createClientSkype(5,8);
        Log.info("Client random date created.");

        driver.findElement(createClientButtonBy).click();

        driver.findElement(firstNameBy).sendKeys(clientNew.get("first_name"));

        driver.findElement(lastNameBy).sendKeys(clientNew.get("last_name"));

        driver.findElement(saveButtonBy).click();

        driver.findElement(emailField).sendKeys(clientNew.get("email"));

        driver.findElement(countryField).sendKeys(clientNew.get("country"));

        driver.findElement(cityField).sendKeys(clientNew.get("city"));

        driver.findElement(phoneNumberField).sendKeys(clientNew.get("phone_number"));

        driver.findElement(skypeField).sendKeys(clientNew.get("skype"));

        driver.findElement(saveCreatedClientButton).click();
        Log.info("New client on site created.");
    }

    //Search client by name and lastname
    public void searchClient() {
        String searchClient = clientNew.get("first_name") + " " + clientNew.get("last_name");
        driver.findElement(searchFieldBy).clear();
        driver.findElement(searchFieldBy).sendKeys(searchClient);

        new WebDriverWait(driver, 5).until(ExpectedConditions.textToBePresentInElementLocated(clientsNameBy,searchClient));

        Assert.assertEquals(searchClient, driver.findElement(clientsNameBy).getText());
        Log.info("Client founded.");
    }

    //Search for client and get client info as Map
    public void getNewClientInfo(){
        searchClient();

        driver.findElement(clientNameToClick).click();

        String actualName = driver.findElement(firstNameBy).getAttribute("value");
        actualInfo.put("first_name",actualName);

        String actualLastname = driver.findElement(lastNameBy).getAttribute("value");
        actualInfo.put("last_name",actualLastname);

        String actualEmail = driver.findElement(emailField).getAttribute("value");
        actualInfo.put("email", actualEmail);

        String actualCountry = driver.findElement(countryField).getAttribute("value");
        actualInfo.put("country", actualCountry);

        String actualCity = driver.findElement(cityField).getAttribute("value");
        actualInfo.put("city", actualCity);

        String actualPhoneNumber = driver.findElement(phoneNumberField).getAttribute("value");
        actualInfo.put("phone_number", actualPhoneNumber);

        String actualSkype = driver.findElement(skypeField).getAttribute("value");
        actualInfo.put("skype", actualSkype);
        Log.info("Client dates saved as Map");

    }


    /*
    Refresh clients page
     */
    public void refreshPage(){
        driver.findElement(refreshButtonBy).click();
    }

    /**
     * Find WebElements in table by clients names
     * Make clients name List
     * @return clients name List
     */
    public List<String> clientsNames() {
        List<WebElement> clientsNames = driver.findElements(clientsNameBy);
        List<String> clientsNameList = new ArrayList<>();
        for (int i = 0; i < clientsNames.size(); i++) {
            clientsNameList.add(clientsNames.get(i).getText());
        }
        Log.info("Clients names saved as List.");
        return clientsNameList;
    }

    /**
     * Find records count field
     * @return text
     */
    public String getRecordsCount(){
        return driver.findElement(clientsRecordCountBy).getText();
    }

    // Change new created client Info
    public void changeClientInfo(){
        createClientName(5);
        createClientCountry(8);
        createClientCity(6);
        createClientPhoneNumber(10);
        createClientSkype(5,8);
        Log.info(" New client date created.");

        driver.findElement(firstNameBy).clear();
        driver.findElement(firstNameBy).sendKeys(clientNew.get("first_name"));

        driver.findElement(countryField).clear();
        driver.findElement(countryField).sendKeys(clientNew.get("country"));


        driver.findElement(cityField).clear();
        driver.findElement(cityField).sendKeys(clientNew.get("city"));

        driver.findElement(phoneNumberField).clear();
        driver.findElement(phoneNumberField).sendKeys(clientNew.get("phone_number"));

        driver.findElement(skypeField).clear();
        driver.findElement(skypeField).sendKeys(clientNew.get("skype"));

        driver.findElement(saveCreatedClientButton).click();
        Log.info("Client date changed.");


    }


    public  void addFunds(){

        // make random amount
        String expectedAmount = String.valueOf(getRandomInt(1)) +" ";


        //make random description
        String randomDescription = getRandomString(5);

        //check in UI fund before test
        int amountBeforeTest = Integer.parseInt(driver.findElement(amountUAHBy).getText());

        // add fund and make screenshot
        driver.findElement(addFundsButtonBy).click();
        driver.findElement(fundsValueBy).clear();
        driver.findElement(fundsValueBy).sendKeys(expectedAmount);
        driver.findElement(descriptionBy).sendKeys(randomDescription);
        ScreenShootCapture.captureScreenShot(driver, "AddFundsTest");
        driver.findElement(saveChangesButtonBy).click();

        //check fund after adding fund
        int amountAfterTest = Integer.parseInt(driver.findElement(amountUAHBy).getText());

        //calculate really added fund
        String actualAmount = (amountAfterTest - amountBeforeTest + " ");

        clientListDB.clear();

        //get info from DB about last fund
       getSelectList(getAmountForFundsTest, 4);

        //get from UI info about client
        String clientName = driver.findElement(firstNameBy).getAttribute("value") + " ";
        String clientLastName = driver.findElement(lastNameBy).getAttribute("value") + " ";
        String currency = driver.findElement(currencyUAH).getText() + " ";



        //assert UI client  and DB client first name
        getAssert(clientName, clientListDB.get(0));

        //assert UI client  and DB client last name
        getAssert(clientLastName, clientListDB.get(1));

        //assert UI and DB fund
        getAssert(actualAmount, clientListDB.get(2));

        //assert UI and DB currency type
        getAssert(currency, clientListDB.get(3));

        //assert UI added fund and really added fund
        getAssert(expectedAmount,actualAmount);
    }




}
