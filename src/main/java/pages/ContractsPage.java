package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;
import utils.Log;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import static config.DataBase.clientListDB;

public class ContractsPage {
    protected WebDriver driver;

    public static List<String> contractClientList = new ArrayList<>();

    private By searchBy = By.xpath("//input[@class = 'form-control']");

    private By clientForSearchBy = By.xpath("//tbody//td/following-sibling::td");

    private By contractsSearchCount = By.xpath("//tbody/tr");


    public  ContractsPage(WebDriver driver){
    this.driver = driver;
    }

    // Change client info from DB to website format
    public List<String> changeBDInfoFormat(){

        //Change client and mentor name/surname format
        String clientName = clientListDB.get(0) + clientListDB.get(1);
        String mentorName = clientListDB.get(3) + clientListDB.get(4);
        clientListDB.remove(4);
        clientListDB.remove(3);
        clientListDB.remove(1);
        clientListDB.remove(0);
        clientListDB.add(0,clientName);
        clientListDB.add(2,mentorName);

        //Change date format
        String oldDateString = clientListDB.get(3);
        SimpleDateFormat oldDateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
        SimpleDateFormat newDateFormat = new SimpleDateFormat("dd-MMM-yyyy", Locale.getDefault());

        Date date = null;
        try {
            date = oldDateFormat.parse(oldDateString);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        String result = newDateFormat.format(date);
        clientListDB.remove(3);
        clientListDB.add(3,result + " ");

        Log.info("Client dates format changed");

        return clientListDB;
    }

    //Search client using BD date
    public void searchClient() {
        changeBDInfoFormat();
        driver.findElement(searchBy).clear();
        driver.findElement(searchBy).sendKeys(clientListDB.get(0));

        new WebDriverWait(driver, 5).until(webDriver -> driver.findElements(contractsSearchCount).size() < 6);

        List<WebElement> clients = driver.findElements(clientForSearchBy);
        int n = 0, m = 6;
        boolean getMatch = false;
        while (!getMatch) {
            for (int i = n; i < m; i++) {
                contractClientList.add(clients.get(i).getText() + " ");
            }

            //If no match -> next loop
            if (!clientListDB.equals(contractClientList)) {
                contractClientList.clear();
                n += 6;
                m += 6;
                if (n >= clients.size()) {
                    Log.error("No such contract");
                    getMatch = true;
                }
            }else {
                getMatch = true;
                Log.info("Contract founded");
            }
        }
    }


}
