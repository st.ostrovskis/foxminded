package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import utils.Log;

import java.util.ArrayList;
import java.util.List;

import static config.DataBase.clientListDB;

public class InvoicesPage {
    protected WebDriver driver;
    public static List<String> clientInvoiceList = new ArrayList<>();

    public static String clientNameForFund;

    private By searchBy = By.xpath("//input[@class ='form-control']");

    private By searchInvoiceCountBy = By.xpath("//table[@class = 'table table-hover']//tbody/tr");

    private By searchInvoiceBy = By.xpath("//table[@class = 'table table-hover']//tbody/tr/td");

    private By clientInfoBy = By.xpath("//table[@data-toggle='table']//tr/td/following-sibling::td/a");




    public InvoicesPage(WebDriver driver){
        this.driver = driver;
    }

    //Change DB date format to UI date format
    public List<String> changeBDInfoFormat(){

        String client = clientListDB.get(1) + clientListDB.get(2);
        String payment = clientListDB.get(4) + clientListDB.get(5);

        clientListDB.remove(5);
        clientListDB.remove(4);
        clientListDB.remove(2);
        clientListDB.remove(1);
        clientListDB.add(1,client);
        clientListDB.add(3,payment);
        if (clientListDB.get(4) == null){
            clientListDB.add(5,"NO");
        }else{
            clientListDB.add(5,"YES ");
        }
        Log.info("Client dates changed");

        return clientListDB;
    }

    //Using DB dates search client on UI
    public List<String> searchClient(){
        changeBDInfoFormat();

        driver.findElement(searchBy).clear();
        driver.findElement(searchBy).sendKeys(clientListDB.get(1));

        new WebDriverWait(driver,5).until(ExpectedConditions.textToBePresentInElementLocated(searchInvoiceCountBy,clientListDB.get(1)));


        List<WebElement> invoices = driver.findElements(searchInvoiceBy);
        int n = 0, m = 6;
        boolean getMatch = false;
        while (!getMatch) {
            for (int i = n; i < m; i++) {
                clientInvoiceList.add(invoices.get(i).getText() + " ");
            }

            //If no match -> next loop
            if (!clientListDB.equals(clientInvoiceList)) {
                clientInvoiceList.clear();
                n += 6;
                m += 6;
                if (n >= invoices.size()) {
                    Log.error("No such invoice");
                    getMatch = true;
                }
            }else {
                getMatch = true;
                Log.info("Invoice founded");
            }
        }
        return clientInvoiceList;
    }

    //Change DB client info
    public List<String> changeDBInfoFormatFirstLastName(){
        String client = clientListDB.get(0) + clientListDB.get(1);

        clientNameForFund = clientListDB.get(0);
        clientListDB.remove(1);
        clientListDB.remove(0);
        clientListDB.add(0, client);

        return clientListDB;
    }

    public ClientsPage clientsPage(){
        changeDBInfoFormatFirstLastName();
        driver.findElement(searchBy).sendKeys(clientListDB.get(0));

        new WebDriverWait(driver,5).until(ExpectedConditions.textToBePresentInElementLocated(searchInvoiceCountBy,clientListDB.get(0)));

        driver.findElement(clientInfoBy).click();

        return new ClientsPage(driver);
    }
}
