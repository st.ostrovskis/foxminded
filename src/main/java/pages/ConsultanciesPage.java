package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import utils.Log;

import java.util.ArrayList;
import java.util.List;

import static utils.GetAssert.getAssert;
import static utils.GetRandom.getRandomInt;
import static utils.GetRandom.getRandomString;
import static utils.ScreenShootCapture.captureScreenShot;

/**
 * Page Object encapsulates the Consultancies page.
 */
public class ConsultanciesPage {
    protected WebDriver driver;

    private By newConsultancyButtonBy = By.xpath("//div[@id = 'fixed-table-toolbar--buttons']/a");

    private By titleField = By.id("name");

    private By descriptionField = By.id("description");

    private By priceUAHField = By.id("prices[0].amount");

    private By priceEURField = By.id("prices[1].amount");

    private By priceUSDBField = By.id("prices[2].amount");

    private By saveButtonBy = By.xpath("//div[@class = 'btn-group']/button[@type = 'submit']");

    private By goBackButtonBy = By.xpath("//div[@class='btn-group']/button");

    private By deleteButtonBy = By.xpath("//div[@class='btn-group col-md-4 col-sm-4 col-xs-3']/button");

    private By employeeRateBy = By.xpath("//input[@id = 'employeeRate.amount']");

    private By searchFieldBy = By.xpath("//div[@class='pull-right search']/input");

    private By refreshButtonBy = By.name("refresh");

    private By consultanciesCountBy = By.className("pagination-info");

    private By  consultanciesTypesBy = By.xpath("//table[@class='table table-hover']/tbody/tr");

    private By consultancyTypeByNameBy = By.xpath("//table[@class='table table-hover']/tbody/tr/td/following-sibling::td/a");

    private By descriptionMessage = By.xpath("//textarea[@id = 'description']/following-sibling::output");

    private By titleMessage = By.xpath("//input[@id = 'name']/following-sibling::output");



    public ConsultanciesPage(WebDriver driver){
        this.driver = driver;
    }

    /*
    Refresh employees page
     */
    public void refreshPage(){
        driver.findElement(refreshButtonBy).click();
    }

    /**
     * Find records count field
     * @return text
     */
    public String getRecordsCount(){
        return driver.findElement(consultanciesCountBy).getText();
    }

    /**
     * Search consultancy by text
     * @return search results
     */
    public void searchConsultancy(String title){
        driver.findElement(searchFieldBy).clear();
        driver.findElement(searchFieldBy).sendKeys(title);

        new WebDriverWait(driver,5).until(ExpectedConditions.textToBePresentInElementLocated(consultancyTypeByNameBy,title));
        String actualResult = driver.findElement(consultancyTypeByNameBy).getText();
        getAssert(title, actualResult);
        Log.info("Consultancy  founded.");

    }

    /**
     * Find WebElements  in table by consultancies types
     * Make consultancies types List
     * @return consultancies types List
     */
    public List<String> getConsultanciesTypes(){
        List<WebElement> webElementList = driver.findElements(consultanciesTypesBy);
        List<String> consultanciesTypes = new ArrayList<>();
        for (int i = 0; i < webElementList.size(); i++){
            consultanciesTypes.add(webElementList.get(i).getText());
        }
        Log.info("Consultancies types founded.");
        return consultanciesTypes;
    }



    /**
     * Create new Consultancy
     */
    public void createNewConsultancy(String title, String description, String priceUAH, String priceEUR,String priceUSD, String employeeRate){

        //Fill all fields
        driver.findElement(newConsultancyButtonBy).click();
        driver.findElement(titleField).sendKeys(title);
        driver.findElement(descriptionField).sendKeys(description);
        driver.findElement(priceUAHField).clear();
        driver.findElement(priceUAHField).sendKeys(priceUAH);
        driver.findElement(priceEURField).clear();
        driver.findElement(priceEURField).sendKeys(priceEUR);
        driver.findElement(priceUSDBField).clear();
        driver.findElement(priceUSDBField).sendKeys(priceUSD);
        driver.findElement(employeeRateBy).sendKeys(employeeRate);
        driver.findElement(saveButtonBy).click();
        Log.info("New consultancy created.");
    }

    public void checkTitleMessage(String expectedTitleMessage){
        String actualTitleMessage = driver.findElement(titleMessage).getText();

        //Assert expected and actual title message
        getAssert(expectedTitleMessage,actualTitleMessage);
        Log.info("Title messages are equal");

    }

    public void checkDescriptionMessage(String expectedDescriptionMessage){
        String actualDescriptionMessage = driver.findElement(descriptionMessage).getText();

        //Assert expected and actual description message
        getAssert(expectedDescriptionMessage, actualDescriptionMessage);
        Log.info("Description messages are equal");

    }










}
