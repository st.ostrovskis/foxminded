package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import utils.Log;

import static utils.GetAssert.getAssert;

/**
 * Page Object encapsulates the Dashboard page.
 */
public class DashboardPage {

    protected WebDriver driver;

    private By dashboardBy = By.xpath("//a[contains(@href, 'admin')]");

    private  By queuesBy = By.xpath("//a[contains(@href, 'queues')]");

    private By clientsBy = By.xpath("//a[contains(@href, 'clients')]");

    private By employeesBy = By.xpath("//a[contains(@href, 'employees')]");

    private By consultanciesBy = By.xpath("//a[contains(@href, 'consultancies')]");

    private By paymentsBy = By.xpath("//a[contains(@href, 'payments')]");

    private By usersBy = By.xpath("//a[contains(@href, 'users')]");

    private By cashflowBy = By.xpath("//a[contains(@href, 'cashflow')]");

    private By dealsBy = By.xpath("//a[contains(@href, 'deals')]/ancestor::li");

    private By dealsArchivedBy = By.xpath("//a[contains(@href, 'deals/archived')]");

    private By dealsAllBy = By.xpath("//a[contains(@href, 'deals/archived')]//ancestor::li/following-sibling::li/a");

    private By contractsBy = By.xpath("//a[contains(@href, 'contracts')]/ancestor::li");

    private By contractsAllBy = By.xpath("//a[contains(@href, 'contracts')]");

    private By contractsTrialBy = By.xpath("//a[contains(@href, 'trial')]");

    private By invoicesBy = By.xpath("//a[contains(@href, 'invoices')]/ancestor::li");

    private By invoicesAllBy = By.xpath("//a[contains(@href, 'invoices')]");

    private By invoicesIssueBy = By.xpath("//a[contains(@href, 'invoices/issue')]");

    private By invoicesDebtBy = By.xpath("//a[contains(@href, 'invoices/debt')]");

    private By salaryBy = By.xpath("//a[contains(@href, 'salary')]");

    private By salaryRollBy = By.xpath("//a[contains(@href, 'payroll')]");

    private By extraFieldButton = By.xpath("//a[contains(@href, 'extra-fields')]");



    public DashboardPage(WebDriver driver){
        this.driver = driver;
    }

    /***
     *Click on employees button
     * @return EmployessPage object
     */
    public EmployeesPage employeesPage(){
        driver.findElement(employeesBy).click();
        Log.info("Employee button clicked in");
        return new EmployeesPage(driver);

    }

    /**
     * Click on clients button
     * @return ClientsPage object
     */
    public ClientsPage clientsPage(){
        driver.findElement(clientsBy).click();
        Log.info("Clients button clicked in ");
        return new ClientsPage(driver);
    }

    /**
     * Click on consultancies button
     * @return ConsultanciesPage object
     */
    public ConsultanciesPage consultanciesPage(){
        driver.findElement(consultanciesBy).click();
        Log.info("Consultancies button clicked in");
        return new ConsultanciesPage(driver);
    }
    /**
     * Click on Contracts button
     * @return ContractsPage object
     */
    public  ContractsPage contractsPage(){
        driver.findElement(contractsBy).click();
        driver.findElement(contractsAllBy).click();
        Log.info("Contracts button clicked in");
        return new ContractsPage(driver);
    }

    /**
     * Click on Invoices button
     * @return InvoicesPage object
     */
    public InvoicesPage invoicesPage(){
        driver.findElement(invoicesBy).click();
        driver.findElement(invoicesAllBy).click();
        Log.info("Invoices button clicked.");
        return new InvoicesPage(driver);
    }

    /**
     * Click on Deals button
     * @return DealsPage object
     */
    public DealsPage dealsPage(){
        driver.findElement(dealsBy).click();
        driver.findElement(dealsAllBy).click();
        return new DealsPage(driver);
    }

    //click on extra fields button
    public ExtraFields extraFields(){
        driver.findElement(extraFieldButton).click();
        Log.info("Click on extra fields button");
        return new ExtraFields(driver);
    }

    //click on cash flow button
    public CashflowPage cashflowPage(){
        driver.findElement(cashflowBy).click();
        Log.info("Click on cash flow button");
        return new CashflowPage(driver);
    }

    //check that page is Dashboard
    public void checkDashboard(){
        String expectedTitle = "Dashboard";
        String actualTitle = driver.findElement(dashboardBy).getText();
        getAssert(expectedTitle,actualTitle);
    }




}
