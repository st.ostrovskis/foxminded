package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import utils.Log;

import static utils.GetAssert.getAssert;
import static utils.GetRandom.getRandomString;

public class ExtraFields {
    protected WebDriver driver;

    private By createNewClientFieldButton = By.xpath("//div[@id='createClientFieldModal']/following-sibling::button");

    private By clientFieldInput = By.id("clientFieldName");

    private By clientFieldSaveButton = By.xpath("//input[@id='clientFieldName']/../following-sibling::button");

    private By clientFieldSearchField = By.xpath("//div[@id='fixed-table-toolbar--buttons-client']/..//div[@class='pull-right search']/input");

    private By clientFieldNameColumn = By.xpath("//div[@id='fixed-table-toolbar--buttons-client']/..//tbody/tr/td/following-sibling::td/p");

    private By clientFieldDeleteButton = By.xpath("//form[@id='deleteClientField']/button");


    public ExtraFields(WebDriver driver){
        this.driver = driver;
    }

    //create new client field
    public void createNewClientField(String newClientFieldName){

        driver.findElement(createNewClientFieldButton).click();
        driver.findElement(clientFieldInput).sendKeys(newClientFieldName);
        driver.findElement(clientFieldSaveButton).click();
        Log.info("Create new client field");
    }

    //search client field
    public void searchClientField(String clientFieldName){
        driver.findElement(clientFieldSearchField).sendKeys(clientFieldName);
        new WebDriverWait(driver,5).until(ExpectedConditions.textToBePresentInElementLocated(clientFieldNameColumn,clientFieldName));

        String expectedResult = clientFieldName;
        String actualResult = driver.findElement(clientFieldNameColumn).getText();
        getAssert(expectedResult,actualResult);
    }

    //delete client field
    public void deleteClientField(String clientFieldName){
        driver.findElement(clientFieldDeleteButton).click();
        driver.switchTo().alert().accept();

        //check that field is really deleted
        driver.findElement(clientFieldSearchField).sendKeys(clientFieldName);
        if(new WebDriverWait(driver,5).until(ExpectedConditions.not
                (ExpectedConditions.textToBePresentInElementLocated(clientFieldNameColumn,clientFieldName)))) {
            Log.info("Delete client field");
        }else{
            Log.error("Cann`t delete client");
        }
    }
}

