package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;
import utils.Log;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import static config.DataBase.clientListDB;

public class DealsPage {
    protected WebDriver driver;
    public static List<String> dealsClientDBList = new ArrayList<>();

    private By searchBy = By.xpath("//input[@class = 'form-control']");

    private By dealsSearchCount = By.xpath("//table[@class = 'table table-hover']//tbody/tr");

    private By clientForSearchBy = By.xpath("//table[@class = 'table table-hover']//tbody/tr/td");

    public DealsPage(WebDriver driver){
        this.driver = driver;
    }

    public static  List<String> changeBDInfoFormat(){

        //Change client name/surname format
        String client = clientListDB.get(2) + clientListDB.get(1);
        clientListDB.remove(2);
        clientListDB.remove(1);
        clientListDB.add(1,client);


        // Check is date null or not
        if(!clientListDB.get(4).equals(clientListDB.get(5))) {
            String oldDateString = clientListDB.get(4);
            SimpleDateFormat oldDateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
            SimpleDateFormat newDateFormat = new SimpleDateFormat("dd MMM yyyy", Locale.getDefault());

            //Change date format
            Date date = null;
            try {
                date = oldDateFormat.parse(oldDateString);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            String result = newDateFormat.format(date);
            clientListDB.remove(5);
            clientListDB.remove(4);
            clientListDB.add(4, result + " - ");
        }else{
            clientListDB.remove(5);
            clientListDB.remove(4);
            clientListDB.add(4, "- ");
        }

        Log.info("Client dates format changed");

        return clientListDB;
    }

    //Using DB dates search client on UI
    public void searchClient(){
        changeBDInfoFormat();
        driver.findElement(searchBy).click();
        driver.findElement(searchBy).sendKeys(clientListDB.get(1));

        new WebDriverWait(driver, 5).until(webDriver-> driver.findElements(dealsSearchCount).size() < 4);

        List<WebElement> clients = driver.findElements(clientForSearchBy);
        int n = 0, m = 5;
        boolean getMatch = false;
        while(!getMatch){
            for (int i = n; i < m; i++){
                dealsClientDBList.add(clients.get(i).getText() + " ");

            }

            //If no match -> next loop
            if(!clientListDB.equals(dealsClientDBList)) {
                dealsClientDBList.clear();
                n += 5;
                m += 5;
                if (n >= clients.size()) {
                    Log.error("No such contract");
                    getMatch = true;
                }
            }else {
                getMatch = true;
                Log.info("Contract founded");

            }
        }
    }
}
