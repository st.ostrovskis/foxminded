package pages;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;
import utils.Log;

import java.util.ArrayList;
import java.util.List;

/**
 * Page Object encapsulates the Employees page.
 */
public class EmployeesPage {
    protected WebDriver driver;

    private By createEmployeeBy = By.xpath("//div[@id = 'fixed-table-toolbar--buttons']/button");

    private By enterFirstNameBy = By.id("firstName");

    private By enterLastNameBy = By.id("lastName");

    private By enterMaxClientsBy = By.id("maxClients");

    private By saveButtonBy = By.xpath("//button[contains(text(), 'save')]");

    private By searchFieldBy = By.xpath("//div[@class='pull-right search']/input");

    private By refreshButtonBy = By.name("refresh");

    private By recordsCountBy = By.className("pagination-info");

    private By employeesNameBy = By.xpath("//table[@class='table table-hover']//span");

    private By employeesWorkloadBy = By.xpath("//table[@class='table table-hover']//td/following-sibling::td/following-sibling::td");

    public EmployeesPage(WebDriver driver){
        this.driver = driver;
    }

    /**
     * Find records count field
     * @return text
     */
    public String getRecordsCount(){
        return driver.findElement(recordsCountBy).getText();
    }

    /***
     * Find WebElement List in table by name column
     * Make Names List
     * @return Names List
     */
    public List<String> getNamesList(){
        List<WebElement> names = driver.findElements(employeesNameBy);
        List<String> namesList = new ArrayList<>();
        for (int i = 0; i < names.size(); i++){
            namesList.add(names.get(i).getText());
        }
        Log.info("Employees  founded.");
        return namesList;
    }

    /***
     * Find WebElement List in table by workload column
     * Make Workload List
     * @return Workload List
     */
    public List<String> getWorkloadList(){
        List<WebElement> workloads = driver.findElements(employeesWorkloadBy);
        List<String> workloadsList = new ArrayList<>();
        for (int i = 0; i <workloads.size(); i++){
            workloadsList.add(workloads.get(i).getText());
        }
        Log.info("Employees workloads  founded.");
        return workloadsList;
    }

    /**Create new employee
     *
     * @param firstname
     * @param lastname
     * @param workload
     */
    public void createEmployees(String firstname, String lastname, String workload){
        driver.findElement(createEmployeeBy).click();
        driver.findElement(enterFirstNameBy).sendKeys(firstname);
        driver.findElement(enterLastNameBy).sendKeys(lastname);
        driver.findElement(enterMaxClientsBy).sendKeys(workload);
        driver.findElement(saveButtonBy).click();
        Log.info("New employee  created.");
    }

    /**
     * Search employee by name and lastname
     * @param searchText
     * @return search results
     */
    public void searchEmployee(String searchText){
        driver.findElement(searchFieldBy).clear();
        driver.findElement(searchFieldBy).sendKeys(searchText);
        new WebDriverWait(driver,5).until(webDriver -> driver.findElements(employeesNameBy).size() == 1);
        String actualResult = driver.findElement(employeesNameBy).getText();
        Assert.assertEquals(searchText, actualResult);
        Log.info("Employee  founded.");


    }

    /*
    Refresh employees page
     */
    public void refreshPage(){
        driver.findElement(refreshButtonBy).click();
    }




}
