package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import utils.Log;

import static config.ReadPropertiesFile.configFileReader;
import static config.ReadPropertiesFile.properties;
import static utils.GetAssert.getAssert;
import static utils.ScreenShootCapture.captureScreenShot;

/**
 * Page Object encapsulates the Login page.
 */
public class LoginPage {
    protected WebDriver driver;

    //<input id="username">
    private By usernameBy = By.id("username");

    //<input id="password">
    private By passwordBy = By.id("password");

    //<input id=login-submit>
    private By submitButton = By.id("login-submit");

    private By registerButton = By.id("register-form-link");

    private By usernameFieldRegistration = By.id("register-username");

    private By emailFieldRegistration = By.id("email");

    private By passwordFieldRegistration = By.id("register-password");

    private By registrationSubmitButton = By.id("register-submit");

    public LoginPage(WebDriver driver){
        this.driver = driver;
    }

    //Find username field and send username
    public void userName(String propUserName){

        configFileReader();

        driver.findElement(usernameBy).clear();
        driver.findElement(usernameBy).sendKeys(properties.getProperty(propUserName));
    }

    //Find password field and send password
    public void password(String propPassword){

        configFileReader();

        driver.findElement(passwordBy).clear();
        driver.findElement(passwordBy).sendKeys(properties.getProperty(propPassword));
    }

    //Click on submit button
    public void submit(){
        driver.findElement(submitButton).submit();
    }

    /**
     * Log in
     * @param propUserName
     * @param propPassword
     * @return Dashboard object
     */
    public DashboardPage login(String propUserName, String propPassword){

        userName(propUserName);
        password(propPassword);
        submit();

        String loginPageTitle = "Foxminded Accounting System";
        String actualTitle = driver.getTitle();

        if(!loginPageTitle.equals(actualTitle)){
            Log.info("Successful logged in");
        }else {
            Log.error("Cann`t logged in on website");
        }
        return new DashboardPage(driver);
    }

    //check email validation message
    public void checkEmailMessage(String expectedEmailMessage){

        //Get validation message
        String actualEmailMessage = driver.findElement(emailFieldRegistration).getAttribute("validationMessage");

        //Assert expected validation message and actual
        getAssert(expectedEmailMessage,actualEmailMessage);
        Log.info("Validation messages are equal");
    }
    public void registration(String username, String email, String password){

        //Click on registration button and fill all fields
        driver.findElement(registerButton).click();
        driver.findElement(usernameFieldRegistration).sendKeys(username);
        driver.findElement(emailFieldRegistration).sendKeys(email);
        driver.findElement(passwordFieldRegistration).sendKeys(password);
        driver.findElement(registrationSubmitButton).click();

    }
}
